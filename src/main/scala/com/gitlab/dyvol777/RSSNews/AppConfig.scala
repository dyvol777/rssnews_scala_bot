package com.gitlab.dyvol777.RSSNews

import com.typesafe.config.{Config, ConfigFactory}
import pureconfig.ConfigSource
import pureconfig.generic.auto._

import scala.concurrent.duration._

object AppConfig {

  final case class AppConfig(tgbot: TgBotActorConfig,
                             articlesUpdater: ArticlesUpdaterConfig,
                             library: LibraryActorConfig)

  final case class ProxyConfig(ip: String, port: Int, user: String, pass: String)

  final case class TgBotActorConfig(token: String, proxy: ProxyConfig, chatsUpdateIntervalSeconds: Int, admins: Set[Long]) {
    def chatsUpdateInterval: FiniteDuration = chatsUpdateIntervalSeconds.seconds
  }

  final case class ArticlesUpdaterConfig(searchNewArticlesIntervalSeconds: Int)

  final case class LibraryActorConfig(stateSaveIntervalMinutes: Int, savesDir: String) {
    def stateSaveInterval: FiniteDuration = stateSaveIntervalMinutes.minutes
  }

  def apply(): AppConfig = config

  def asUntyped: Config = untyped

  private lazy val untyped: Config = {
    val local = "local.conf"
    ConfigFactory.load(if (isResourceExists(local)) local else "application.conf")
  }

  private lazy val config: AppConfig = {

    val loaded = ConfigSource.fromConfig(untyped).load[AppConfig]
    assert(loaded.isRight, s"can't load config: $loaded")
    loaded.right.get
  }

  private def isResourceExists(name: String): Boolean = getClass.getClassLoader.getResource(name) != null
}
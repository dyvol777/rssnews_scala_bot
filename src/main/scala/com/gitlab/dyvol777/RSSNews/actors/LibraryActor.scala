package com.gitlab.dyvol777.RSSNews.actors

import java.util
import java.util.Calendar

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.gitlab.dyvol777.RSSNews.AppConfig.LibraryActorConfig
import com.gitlab.dyvol777.RSSNews.actors.ArticlesUpdaterActor.{AddArticle, GetState}
import com.gitlab.dyvol777.RSSNews.actors.LibraryActor._
import com.gitlab.dyvol777.RSSNews.actors.TgBotActor.SendMessageToTg
import com.gitlab.dyvol777.RSSNews.models._
import com.gitlab.dyvol777.RSSNews.utils.Saver

import scala.concurrent.ExecutionContextExecutor


object LibraryActor {
  def props(config: LibraryActorConfig, saver: Saver[State]): Props = Props(new LibraryActor(config, saver))

  final case class GetSettings(chatId: Long)

  final case class UpdateChat(chatId: Long, updater: Chat => Chat, needConfirmation: Boolean)

  final case class SendArticle(chatId: Long, article: Article)

  final case class GetNewArticles()

  final case class SaveState(needConfirmation: Boolean)

  final case class GiveState(state: State)

  final case object Ok

}

class LibraryActor(config: LibraryActorConfig, saver: Saver[State]) extends Actor with ActorLogging {
  implicit val executionContext: ExecutionContextExecutor = context.dispatcher

  private val state = saver.load()

  override def preStart(): Unit = {
    context.system.scheduler.schedule(config.stateSaveInterval, config.stateSaveInterval, self, SaveState(needConfirmation = false))
  }

  override def receive: Receive = {
    case UpdateChat(chatId, updater, needConfirmation) =>
      state.updateChat(chatId)(updater)
      if (needConfirmation) {
        sender ! Ok
      }
    case GetSettings(chatId) =>
      sender ! SendMessageToTg(chatId, state.getChat(chatId).getSettingsAsCmd)
    case GetNewArticles() =>
      processNewArticles(sender)
    case SaveState(needConfirmation) =>
      saver.save(state)
      if (needConfirmation) {
        sender ! Ok
      }
    case GetState() => sender ! GiveState(state)
    case AddArticle(url, article) =>
      log.debug(s"Added ${article.title} from $url")
      state.articles.update(
        url,
        article :: state.articles(url)
      )

    case unknownMessage =>
      log.error(s"Library_Actor unknown message: $unknownMessage")
  }


  private def processNewArticles(tgBot: ActorRef): Unit = {
    for ((id, chat) <- state.chats; (rssName, articles) <- state.articles) {
      if (chat.subscriptions.contains(rssName))
        articles.
          filter(_.publicationDate.after(chat.lastUpdate)).
          foreach(tgBot ! SendArticle(id, _))

    }
    for ((id, chat) <- state.chats) {
      state.chats(id) = Chat(chat.id, chat.subscriptions, Calendar.getInstance().getTime)
    }

  }
}

package com.gitlab.dyvol777.RSSNews.actors

import java.io.{FileNotFoundException, IOException}
import java.net.{SocketException, UnknownHostException}
import java.util.concurrent.Executors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.gitlab.dyvol777.RSSNews.AppConfig.ArticlesUpdaterConfig
import com.gitlab.dyvol777.RSSNews.actors.LibraryActor.GiveState
import com.gitlab.dyvol777.RSSNews.loaders.RssLoader
import com.gitlab.dyvol777.RSSNews.models.Article

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}


object ArticlesUpdaterActor {
  def props(config: ArticlesUpdaterConfig, library: ActorRef): Props =
    Props(new ArticlesUpdaterActor(config, library))

  final case class SearchNewArticles()
  final case class GetState()
  final case class AddArticle(url: String, article: Article)
}

class ArticlesUpdaterActor private(config: ArticlesUpdaterConfig, library: ActorRef) extends Actor with ActorLogging {

  import ArticlesUpdaterActor._

  private val threadsCount = 4
  private implicit val executionContext: ExecutionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(threadsCount))

  override def preStart(): Unit = {
    context.system.scheduler.schedule(0.second, config.searchNewArticlesIntervalSeconds.seconds, self, SearchNewArticles())
  }

  override def receive: Receive = {
    case SearchNewArticles() =>
      log.debug("Asked for new articles, getting state...")
      library ! GetState()
    case GiveState(state) =>
      log.debug(s"Get state: $state")
      for((id, chat)<- state.chats; url <- chat.subscriptions)
        state.articles.getOrElseUpdate(url, List.empty)
      for((rss, articles) <- state.articles){
        searchNewArticles(rss, articles)
      }

  }

  private def doSafe[T](func: => T, default: => T, handleException: Throwable => Unit): T =
    Try(func) match {
      case Success(result) =>
        result
      case Failure(exception) =>
        handleException(exception)
        default
    }

  private def logException(text: String, printTrace: Throwable => Boolean): Throwable => Unit =
    ex => log.error(s"$text $ex ${if (printTrace(ex)) ex.getStackTrace.mkString("\n") else ""}")

  private def isInterestingException: Throwable => Boolean = {
    case _: UnknownHostException => false
    case _: SocketException => false
    case _: FileNotFoundException => false
    case _: IOException => false
    case _ => true
  }

  def searchNewArticles(url: String, articles: List[Article]): Unit = Future {
    for {
      article <- doSafe(RssLoader.downloadRSSArticles(url), Seq(), logException("can't download rss articles", isInterestingException))
    } {
      log.info(s"add new article: ${article.link}")
      if(!articles.contains(article))
        library ! AddArticle(url, article)
    }
  }
}

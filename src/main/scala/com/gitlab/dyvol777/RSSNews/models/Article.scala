package com.gitlab.dyvol777.RSSNews.models

import java.util.Date

import com.gitlab.dyvol777.RSSNews.utils.DateUtils._

import io.circe.syntax._
import io.circe.{Decoder, Encoder, HCursor, Json}

case class Article(link: String,
                   title: String,
                   description: String,
                   publicationDate: Date) {
  override def hashCode(): Int = link.hashCode

  override def equals(obj: Any): Boolean =
    obj match {
      case art: Article => art.title == this.title
      case _ => false
    }

  override def toString: String =
    title + "\n\r" +
      description + "\n\r" +
      link
}

object Article {
  implicit val encoder: Encoder[Article] = (article: Article) => {
    Json.obj(
      "link" := article.link,
      "title" := article.title,
      "description" := article.description,
      "publicationDate" := article.publicationDate
    )
  }

  implicit val decoder: Decoder[Article] = (c: HCursor) => {
    for {
      link <- c.get[String]("link")
      title <- c.get[String]("title")
      description <- c.get[String]("description")
      publicationDate <- c.get[Date]("publicationDate")
    } yield Article(link, title, description, publicationDate)
  }
}
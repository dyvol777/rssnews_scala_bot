package com.gitlab.dyvol777.RSSNews.models

import com.gitlab.dyvol777.RSSNews.utils.DateUtils.{dateDecoder, dateEncoder}
import java.util.{Calendar, Date}

import io.circe._
import io.circe.syntax._


case class Chat(id: Long,
                subscriptions: List[String] = List.empty,
                lastUpdate: Date = Calendar.getInstance().getTime) {

  def getSettingsAsCmd: String =
    "All subs: " + subscriptions.toString()
}

object Chat {
  def withDefaultSettings(id: Long): Chat =
    Chat(id)

  implicit val encoder: Encoder[Chat] = (chat: Chat) =>
    Json.obj(
      "id" := chat.id,
      "subscriptions" := chat.subscriptions,
      "lastUpdate" := chat.lastUpdate,
    )

  implicit val decoder: Decoder[Chat] = (c: HCursor) => {
    for {
      id <- c.get[Long]("id")
      filterSettings <- c.get[List[String]]("subscriptions")
      sentPosts <- c.get[Date]("lastUpdate")
    } yield Chat(id, filterSettings, sentPosts)
  }
}

# RSSNews Scala Bot

Чат бот для телеграмма.
Можно подписываться на rss-каналы и получать новости с них от бота

* Основано на https://github.com/Kright/habrahabr_reader
* Переписано для работы с любыми rss новостями
* Содержит проблемы, такие как парсинг даты и прочее.
* (написано за один день, да, я слегка разгильдяй)
